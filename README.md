# Baza danych dla firmy remontowej
Aplikacja będzie składała się z bazy danych oraz
narzędzia do tworzenia automatycznych raportów.

# Uruchomienie aplikacji
Aby wykorzystać aplikację, należy
ustawić zmienną środowiskową `MYSQL_ROOT_PASSWORD` na wybrane przez siebie hasło,
a następnie wewnątrz folderu z tym repozytorium uruchomić polecenie
```
docker-compose up -d
```
Pozwoli to na uruchomienie bazy danych i utworzenie niezbędnych tabel.

### Przydatne polecenia

* Zatrzymanie aplikacji: `docker-compose stop`
* Wznowienie aplikacji: `docker-compose start`
* Całkowite usunięcie działającej aplikacji: `docker-compose down`

# Łączenie z bazą danych
## Z lokalnej maszyny
Aby połączyć się z bazą danych, należy skorzystać z polecenia:
```
docker-compose port db 3306
```
Port, który zostanie przedstawiony można wykorzystać do
połączenia się z bazą danych w standardowy sposób, za
pomocą ulubionych narzędzi (np. ApexSQL).
## Z kontenera służącego do analizy
Baza danych jest dostępna pod adresem `db:3306`.
Można z niej korzystać w dowolny, wybrany przez
siebie, sposób

# Generowanie danych testowych
Aby wygenerować dane testowe, należy skorzystać z polecenia:
```
docker-compose exec python python ./src/fill_database.py
```

# Analizowanie danych
W celu ułatwienia interaktywnej analizy danych, można wykorzystać link wygenerowny przez polecenie:
```
python3 ./scripts/jupyter_link.py
```
Analiza odpowiada na pytania o:
* liczbę zleceń oczekujących w czasie, +
* liczbę czynnych pracowników w czasie, +
* bilans finansowy, +
* listę osób najdłużej oczekujących na remont, +
* najczęsciej świadczone usługi, +
* zmianę zarobków w czasie, +
* najczęściej wykorzystywany sprzęt, +
* zmianę cen usług w czasie, +
* średni czas oczekiwania na zlecenie, jednolinijkowe
* średni czas realizacji zlecenia, jednolinijkowe
* regiony, w których realizowanych jest najwięcej zleceń, (potrzebny adres)
* średni zysk ze zleceń, jednolinijkowe
* klienta, który najczęściej korzysta z usług firmy, jednolinijkowe
* klienta, który przyniósł najwięcej zysków. jednolinijkowe

# Przygotowanie raportu
Aby wygenerować raport na podstawie bazy danych, należy skorzystać z polecenia:
```
docker-compose exec python python ./src/generate_report.py
```

# O projekcie
Projekt tworzony w ramach kursu Bazy Danych w semestrze zimowym 2020/2021
na podstawie [wytycznych](http://prac.im.pwr.edu.pl/~giniew/doku.php?id=rok2021:letni:bd:proj) prowadzącego.
