CREATE DATABASE IF NOT EXISTS renovations;

USE renovations;

CREATE TABLE employees
(employee_id INT NOT NULL AUTO_INCREMENT,
 name        VARCHAR(20),
 surname     VARCHAR(30),
 zip_code    VARCHAR(8),
 district    VARCHAR(20),
 street      VARCHAR(40),
 birth_date  DATE,
 start_date  DATE,
 end_date    DATE,
 PRIMARY KEY(employee_id)
);

CREATE TABLE clients
(client_id INT NOT NULL AUTO_INCREMENT,
 name      VARCHAR(20),
 surname   VARCHAR(30),
 telephone INT,
 PRIMARY KEY(client_id)
);

CREATE TABLE salaries
(salary_id    INT NOT NULL AUTO_INCREMENT,
 employee_id  INT,
 salary       DECIMAL,
 payment_date DATE,
 FOREIGN KEY(employee_id) REFERENCES employees(employee_id),
 PRIMARY KEY(salary_id)
);

CREATE TABLE transaction_types
(trans_type_id    INT NOT NULL AUTO_INCREMENT,
 transaction_type VARCHAR(50),
 PRIMARY KEY(trans_type_id)
);

CREATE TABLE transactions
(trans_id      INT NOT NULL AUTO_INCREMENT,
 trans_date    DATE,
 trans_amount  DECIMAL,
 trans_type_id INT,
 PRIMARY KEY(trans_id),
 FOREIGN KEY(trans_type_id) REFERENCES transaction_types(trans_type_id)
);

CREATE TABLE orders
(order_id   INT NOT NULL AUTO_INCREMENT,
 client_id  INT,
 order_date DATE,
 zip_code   VARCHAR(8),
 district   VARCHAR(20),
 street     VARCHAR(40),
 PRIMARY KEY(order_id),
 FOREIGN KEY(client_id) REFERENCES clients(client_id)
);

CREATE TABLE services_types
(service_type_id INT NOT NULL AUTO_INCREMENT,
 service_type    VARCHAR(50),
 PRIMARY KEY(service_type_id)
);

CREATE TABLE services
(service_id      INT NOT NULL AUTO_INCREMENT,
 order_id        INT,
 price           DECIMAL,
 start_date      DATE,
 end_date        DATE,
 warranty_end    DATE,
 service_type_id INT,
 FOREIGN KEY(order_id) REFERENCES orders(order_id),
 FOREIGN KEY(service_type_id) REFERENCES services_types(service_type_id),
 PRIMARY KEY(service_id)
);

CREATE TABLE equipment_types
(equipment_type_id INT NOT NULL AUTO_INCREMENT,
 equipment_type    VARCHAR(30),
 PRIMARY KEY(equipment_type_id)
);

CREATE TABLE equipment
(equipment_id   INT NOT NULL AUTO_INCREMENT,
 equipment_type_id INT,
 PRIMARY KEY(equipment_id),
 FOREIGN KEY(equipment_type_id) REFERENCES equipment_types(equipment_type_id)
);

CREATE TABLE equipment_services
(equip_serv_id INT NOT NULL AUTO_INCREMENT,
 equipment_id INT,
 service_id   INT,
 FOREIGN KEY(service_id) REFERENCES services(service_id),
 FOREIGN KEY(equipment_id) REFERENCES equipment(equipment_id),
 PRIMARY KEY(equip_serv_id)
);

CREATE TABLE employees_services
(emp_serv_id INT NOT NULL AUTO_INCREMENT,
 employee_id INT,
 service_id  INT,
 FOREIGN KEY(employee_id) REFERENCES employees(employee_id),
 FOREIGN KEY(service_id) REFERENCES services(service_id),
 PRIMARY KEY(emp_serv_id)
);
