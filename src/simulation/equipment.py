from dataclasses import dataclass, field

import pandas as pd
import numpy as np

RANDOM_SEED = 2020

EQUIPMENT_DF = pd.DataFrame(
    {
        "equipment_id": [],
        "equipment_type": []
    }
)

EQUIPMENT_SERVICES_DF = pd.DataFrame(
    {
        "equipment_id": [],
        "service_id": []
    }
)

ADDITIONAL_INFO = pd.DataFrame(
    {
        "equipment_id": [],
        "used_to": []
    }
)

# LOADING SERVICES DETAILS

services_info = pd.read_csv('src/simulation/services.csv')\
    .reset_index()\
    .rename({"index": "service_type_id"}, axis='columns')
services_info["service_type_id"] += 1

SERVICE_TYPE_ID_TO_TYPE = dict(
    zip(
        services_info["service_type_id"],
        services_info["service_type"]
    )
)

SERVICE_TYPE_TO_EQ = dict(
    zip(
        services_info["service_type"],
        services_info["required_equipment"].apply(lambda x: x.split(', '))
    )
)

# LOADING EQIPMENT DETAILS

equipment_info = pd.read_csv('src/simulation/equipment.csv')\
    .reset_index()\
    .rename({"index": "equipment_type_id"}, axis='columns')
equipment_info["equipment_type_id"] += 1

EQ_TYPE_TO_TYPE_ID = dict(
    zip(
        equipment_info["equipment_type"],
        equipment_info["equipment_type_id"]
    )
)

EQ_TYPE_TO_PRICE = dict(
    zip(
        equipment_info["equipment_type"],
        equipment_info["equipment_price"]
    )
)

EQUIPMENT_TYPES_DF = equipment_info.drop("equipment_price", axis="columns")


@dataclass
class Equipment:
    df: pd.DataFrame = EQUIPMENT_DF
    eq_serv_df: pd.DataFrame = EQUIPMENT_SERVICES_DF
    eq_types_df: pd.DataFrame = EQUIPMENT_TYPES_DF
    additional_info: pd.DataFrame = ADDITIONAL_INFO
    rng = np.random.default_rng(RANDOM_SEED)
    service_type_id_to_type: dict = field(
        default_factory=lambda: SERVICE_TYPE_ID_TO_TYPE
    )
    service_type_to_eq: dict = field(
        default_factory=lambda: SERVICE_TYPE_TO_EQ
    )
    eq_type_to_price: dict = field(
        default_factory=lambda: EQ_TYPE_TO_PRICE
    )
    eq_type_to_type_id: dict = field(
        default_factory=lambda: EQ_TYPE_TO_TYPE_ID
    )

    verbose: bool = True

    def find_equipment(self, equipment_type, available_date):
        if self.df.empty:
            return pd.DataFrame()

        same_name = self.df['equipment_type'] == equipment_type
        is_available = self.additional_info['used_to'] <= str(available_date)

        return self.df[same_name & is_available]

    def update_equipment(self, service_details: pd.Series):
        service_type = self.service_type_id_to_type[
            service_details.service_type_id
        ]
        required = self.service_type_to_eq[service_type]

        picked_ids = []
        eq_cost = 0
        for required_item_name in required:
            all_found = self.find_equipment(
                equipment_type=required_item_name,
                available_date=service_details.start_date
            )

            if all_found.empty:
                if self.verbose:
                    print(
                        f'Did not found equipment "{required_item_name}"'
                        f' for service "{service_details.service_id}"'
                    )
                item_id = self._add_eq(required_item_name)
                eq_cost += self.generate_eq_cost(required_item_name)

            else:
                if self.verbose:
                    print(
                        f'Found {required_item_name} for service'
                        f' {service_details.service_id}'
                    )
                item_id = all_found.iloc[0]['equipment_id']

            picked_ids.append(item_id)

        if self.verbose:
            print(f'Picked equipment: {picked_ids}')

        self.update_availability(picked_ids, service_details.end_date)

        new_eq_service_conections = pd.DataFrame(
            {
                "equipment_id": picked_ids,
                "service_id": [service_details.service_id] * len(picked_ids)
            }
        )
        self.eq_serv_df = self.eq_serv_df.append(
            new_eq_service_conections,
            ignore_index=True
        )

        return eq_cost

    def _add_eq(self, new_type):
        new_id = len(self.df) + 1

        if self.verbose:
            print(
                f'Adding item:\n'
                f'\tequipment_id: {new_id}\n'
                f'\tequipment_type:{new_type}'
            )

        self.df = self.df.append(
            {
                "equipment_id": new_id,
                "equipment_type": new_type
            },
            ignore_index=True
        )

        self.additional_info = self.additional_info.append(
            {
                "equipment_id": new_id,
                "used_to": '-9999-00-00'
            },
            ignore_index=True
        )

        return new_id

    def update_availability(self, picked_ids, service_end):
        where_id = self.additional_info['equipment_id'].apply(
            lambda id: id in picked_ids
        )
        self.additional_info.loc[where_id, 'used_to'] = service_end

    def generate_eq_cost(self, eq_type):
        return np.abs(self.rng.normal(self.eq_type_to_price[eq_type], 5))

    def prepare_tables(self):
        self.df["equipment_type_id"] = self.df["equipment_type"]\
            .apply(lambda x: self.eq_type_to_type_id[x])

        self.df = self.df.drop("equipment_type", axis="columns")

        self.eq_serv_df = self.eq_serv_df\
            .reset_index()\
            .rename({"index": "equip_serv_id"}, axis="columns")
        self.eq_serv_df["equip_serv_id"] += 1

        print(self.eq_serv_df)
