from dataclasses import dataclass, field

import pandas as pd


TRANSACTIONS_DF = pd.DataFrame(
    {
        'trans_id': [],
        'trans_date': [],
        'trans_type_id': [],
        'trans_amount': [],
    }
)


@dataclass
class Transactions:
    df: pd.DataFrame = TRANSACTIONS_DF
    trans_types: dict = field(
        default_factory=lambda: []
    )
    verbose: bool = True

    def add_transaction(self, trans_type, date, amount):
        self.df = self.df.append(
            {
                'trans_id': len(self.df) + 1,
                'trans_date': date,
                'trans_type_id': self.trans_types.index(trans_type) + 1,
                'trans_amount': amount,
            },
            ignore_index=True
        )

    def add_transactions(self, trans_type, date, amounts, sign='-'):
        if trans_type not in self.trans_types:
            self.trans_types.append(trans_type)

        sign = {'+': 1, '-': -1}[sign]
        for amount in amounts:
            self.add_transaction(
                trans_type=trans_type,
                date=date,
                amount=amount * sign
            )

    @property
    def trans_types_df(self):
        return pd.DataFrame(
            {
                'trans_type_id': list(range(1, len(self.trans_types) + 1)),
                'transaction_type': self.trans_types
            }
        )
