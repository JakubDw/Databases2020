from dataclasses import dataclass

import pandas as pd

ORDERS_DF = pd.DataFrame(
    {
        "order_id": [],
        "client_id": [],
        "order_date": [],
    }
)


@dataclass
class Orders:
    df: pd.DataFrame = ORDERS_DF
    verbose: bool = True

    def get_new_order_ids(self, client_ids, order_date):
        new_order_ids = []
        for client_id in client_ids:
            new_order_id = self.get_new_order_id(client_id, order_date)
            new_order_ids.append(new_order_id)
        return new_order_ids

    def get_new_order_id(self, client_id, order_date):
        new_order_id = len(self.df) + 1
        new_order = {
            "order_id": new_order_id,
            "client_id": client_id,
            "order_date": str(order_date),
        }
        self.df = self.df.append(new_order, ignore_index=True)
        if self.verbose:
            print("New order:", new_order)
        return new_order_id

    def prepare_tables(self):
        n = len(self.df)
        adresses = pd.read_csv('src/simulation/addresses.csv')\
            .sample(n, replace=True)
        self.df["zip_code"] = adresses["kod_pocztowy"].values
        self.df["district"] = adresses["dzielnica"].values
        self.df["street"] = adresses["ulica"].values
