from dataclasses import dataclass, field
import datetime

import holidays
import numpy as np

from .clients import Clients
from .employees import Employees
from .orders import Orders
from .salaries import accumulate, Salaries
from .services import Services
from .equipment import Equipment
from .transactions import Transactions

RANDOM_SEED = 2020
NEW_ORDERS_FACTOR = 0.5
NEW_EMPLOYEES_FACTOR = 1 / 4
BONUS_RATIO = 1 / 20
SATURDAY_WEEKDAY = 5


@dataclass
class Simulation:
    date: datetime.date
    orders: Orders = field(default_factory=Orders)
    clients: Clients = field(default_factory=Clients)
    services: Services = field(default_factory=Services)
    salaries: Salaries = field(default_factory=Salaries)
    employees: Employees = field(default_factory=Employees)
    equipment: Equipment = field(default_factory=Equipment)
    transactions: Transactions = field(default_factory=Transactions)

    rng = np.random.default_rng(RANDOM_SEED)
    holidays = holidays.POL()
    verbose = True

    def simulate(self, n_days):
        for n_day in range(n_days):
            self.simulate_day()

    def simulate_day(self):
        if self.verbose:
            print("-" * 20, f"Day: {self.date}", sep="\n")
        if self.is_working_day():
            self.simulate_working_day()
        if self.is_end_of_month():
            self.end_of_month()
        self.date += datetime.timedelta(days=1)

    def simulate_working_day(self):
        self.add_new_orders()
        self.start_services()
        self.finish_services()

    def end_of_month(self):
        if self.verbose:
            print("*" * 20, f"End of month: {self.date}", sep="\n")

        self.pay_salaries()
        self.hire_employees()

        if self.verbose:
            print("*" * 20)

    def add_new_orders(self):
        n_new_orders = self.get_n_new_orders()
        new_client_ids = self.clients.get_new_client_ids(n_new_orders)
        new_order_ids = self.orders.get_new_order_ids(
            new_client_ids, self.date
        )
        for new_order_id in new_order_ids:
            self.services.add_new_services(new_order_id)

    def start_services(self):
        available_employee_ids = self.get_available_employee_ids()
        not_started_service_ids = self.services.get_not_started_service_ids(
            self.date
        )
        for employee_id, service_id in zip(
            available_employee_ids,
            not_started_service_ids,
        ):
            self.start_service(employee_id, service_id)

    def finish_services(self):
        ending_today_ids = self.services.get_finished_service_ids(self.date)
        ending_today_ids = ending_today_ids.tolist()
        prices = [self.services.get_price(id) for id in ending_today_ids]

        self.transactions.add_transactions(
            trans_type='Zapłata za zamówienie',
            date=self.date,
            amounts=prices,
            sign='+'
        )

    def pay_salaries(self):
        monthly_bonuses = self.get_employees_month_bonuses()
        base_salaries = self.employees.get_salaries(self.date)

        salaries = {}
        for employee_id, salary in base_salaries.items():
            salaries[employee_id] = salary + monthly_bonuses[employee_id]

        salaries = self.salaries.pay_salaries(salaries, self.date)

        self.transactions.add_transactions(
            trans_type='Wynagrodzenie',
            date=self.date,
            amounts=salaries
        )

    def get_employees_month_bonuses(self):
        start_of_month = self.date.replace(day=1)
        month_service_ids = self.services.get_service_ids_ended_between(
            start_of_month, self.date
        )
        services_incomes = self.services.get_prices(month_service_ids)
        bonuses = [
            BONUS_RATIO * service_income
            for service_income in services_incomes
        ]
        employee_ids = self.employees.get_working_employee_ids(
            month_service_ids
        )
        return accumulate(employee_ids, bonuses)

    def get_n_new_employees(self):
        return self.rng.poisson(NEW_EMPLOYEES_FACTOR)

    def hire_employees(self):
        n_new_employees = self.get_n_new_employees()
        self.employees.hire(n_new_employees, self.date)

    def start_service(self, employee_id, service_id):
        self.services.start_service(service_id, self.date)
        self.employees.start_service(employee_id, service_id)

        service_details = self.services.get_service_details(service_id)
        new_equipment_cost = self.equipment.update_equipment(service_details)

        if new_equipment_cost > 0:
            self.transactions.add_transactions(
                trans_type='Zakup sprzętu',
                date=self.date,
                amounts=[new_equipment_cost]
            )

    def get_available_employee_ids(self):
        actual_services = self.services.get_actual_service_ids(self.date)
        return self.employees.get_available_employee_ids(
            self.date, actual_services
        )

    def get_n_new_orders(self):
        n_employees = self.employees.get_n_actual_employees(self.date)
        n_orders = self.services.get_n_actual_services(self.date)
        return self.rng.poisson(
            n_employees / (n_orders + 1) * NEW_ORDERS_FACTOR
        )

    def is_end_of_month(self):
        return (self.date + datetime.timedelta(days=1)).day == 1

    def is_working_day(self):
        if self.date.weekday() >= SATURDAY_WEEKDAY:
            if self.verbose:
                print("Not working:", self.date.strftime("%A"))
            return False
        if self.date in self.holidays:
            if self.verbose:
                print("Holiday:", self.holidays[self.date])
            return False
        return True

    def get_tables(self):
        self.equipment.prepare_tables()
        self.orders.prepare_tables()
        self.employees.prepare_tables()

        return [
            ("employees", self.employees.df),
            ("clients", self.clients.df),
            ("salaries", self.salaries.df),
            ("orders", self.orders.df),
            ("services_types", self.services.types_df),
            ("services", self.services.df),
            ("employees_services", self.employees.df_services),
            ("equipment_types", self.equipment.eq_types_df),
            ("equipment", self.equipment.df),
            ("equipment_services", self.equipment.eq_serv_df),
            ("transaction_types", self.transactions.trans_types_df),
            ("transactions", self.transactions.df)
        ]
