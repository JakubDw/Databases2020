from dataclasses import dataclass
from datetime import date, timedelta

import numpy as np
import pandas as pd

from .names import NameGenerator

EMPLOYEES_RANDOM_SEED = 202020
DEFAULT_SALARY = 3000

EMPLOYEES_DF = pd.DataFrame(
    {
        "employee_id": [1],
        "name": ["Jan"],
        "surname": ["Kowalski"],
        "birth_date": ["1984-12-12"],
        "start_date": ["2018-01-01"],
        "end_date": [None],
    }
)

EMPLOYEES_SERVICES_DF = pd.DataFrame(
    {
        "emp_serv_id": [],
        "employee_id": [],
        "service_id": [],
    }
)


def generate_birth_date(
    hire_date, rng=np.random.default_rng(EMPLOYEES_RANDOM_SEED)
):
    age = int(rng.integers(20, 60))
    additional_days = int(rng.integers(0, 366))
    base_date = date(
        hire_date.year - age,
        hire_date.month,
        hire_date.day - 1
    )
    return base_date - timedelta(days=additional_days)


@dataclass
class Employees:
    df: pd.DataFrame = EMPLOYEES_DF
    df_services: pd.DataFrame = EMPLOYEES_SERVICES_DF
    verbose: bool = True
    name_gen: NameGenerator = NameGenerator(EMPLOYEES_RANDOM_SEED)
    employees_salaries = {1: DEFAULT_SALARY}

    def hire(self, n_new_employees, hire_date):
        _ = self.get_new_employee_ids(n_new_employees, hire_date)

    def get_n_actual_employees(self, date):
        return len(self.get_hired_employee_ids(date))

    def get_hired_employee_ids(self, date):
        is_hired = (
            (self.df["start_date"] <= str(date)) &
            ((self.df["end_date"] >= str(date)) | self.df["end_date"].isna())
        )
        return self.df[is_hired]["employee_id"]

    def get_working_employee_ids(self, service_ids):
        employee_ids = []
        for service_id in service_ids:
            employee_ids.append(self.get_working_employee_id(service_id))
        return employee_ids

    def get_working_employee_id(self, service_id):
        service_id_mask = (self.df_services["service_id"] == service_id)
        if service_id_mask.any():
            return self.df_services[service_id_mask]["employee_id"].squeeze()
        else:
            return None

    def get_available_employee_ids(self, date, actual_service_ids):
        hired_employee_ids = self.get_hired_employee_ids(date)
        working_employee_ids = self.get_working_employee_ids(
            actual_service_ids
        )
        return set(hired_employee_ids) - set(working_employee_ids)

    def start_service(self, employee_id, service_id):
        emp_serv_id = len(self.df_services) + 1
        new_emp_serv = {
            "emp_serv_id": emp_serv_id,
            "employee_id": employee_id,
            "service_id": service_id,
        }
        self.df_services = self.df_services.append(
            new_emp_serv, ignore_index=True
        )
        if self.verbose:
            print("New emp_serv:", new_emp_serv)

    def get_new_employee_ids(self, n_new_employees, hire_date):
        new_employee_ids = []
        for _ in range(n_new_employees):
            new_employee_id = self.get_new_employee_id(hire_date)
            new_employee_ids.append(new_employee_id)
        return new_employee_ids

    def get_new_employee_id(self, hire_date):
        new_employee_id = len(self.df) + 1
        name, surname = self.name_gen.generate_name_surname(male_chance=.95)
        birth_date = generate_birth_date(hire_date)
        new_employee = {
            "employee_id": new_employee_id,
            "name": name,
            "surname": surname,
            "start_date": str(hire_date),
            "end_date": None,
            "birth_date": str(birth_date),
        }
        self.employees_salaries[new_employee_id] = self.get_new_salary()
        self.df = self.df.append(new_employee, ignore_index=True)
        if self.verbose:
            print("New employee:", new_employee)
        return new_employee_id

    def get_new_salary(self):
        return DEFAULT_SALARY

    def get_salaries(self, date):
        return {
            employee_id: self.get_salary(employee_id)
            for employee_id in self.get_hired_employee_ids(date)
        }

    def get_salary(self, employee_id):
        return self.employees_salaries[employee_id]

    def prepare_tables(self):
        n = len(self.df)
        adresses = pd.read_csv('src/simulation/addresses.csv')\
            .sample(n, replace=True)
        self.df["zip_code"] = adresses["kod_pocztowy"].values
        self.df["district"] = adresses["dzielnica"].values
        self.df["street"] = adresses["ulica"].values
