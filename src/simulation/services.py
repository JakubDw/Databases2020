from dataclasses import dataclass, field
import datetime

import numpy as np
import pandas as pd

RANDOM_SEED = 2020

SERVICES_DF = pd.DataFrame(
    {
        "service_id": [],
        "order_id": [],
        "price": [],
        "start_date": pd.to_datetime([]),
        "end_date": pd.to_datetime([]),
        "service_type_id": [],
    }
)

services_info = pd.read_csv('src/simulation/services.csv')\
    .reset_index()\
    .rename({"index": "service_type_id"}, axis="columns")
services_info["service_type_id"] += 1

SERVICES_TYPES_DF = services_info.drop(
    [
        "required_equipment",
        "service_price"
    ],
    axis="columns"
)

SERVICE_TYPE_ID_TO_PRICE = dict(
    zip(
        services_info["service_type_id"],
        services_info["service_price"]
    )
)


@dataclass
class Services:
    df: pd.DataFrame = SERVICES_DF
    types_df: pd.DataFrame = SERVICES_TYPES_DF
    verbose: bool = True
    rng = np.random.default_rng(RANDOM_SEED)
    service_type_id_to_price: dict = field(
        default_factory=lambda: SERVICE_TYPE_ID_TO_PRICE
    )

    def add_new_services(self, order_id):
        n_new_services = self.get_n_of_new_services()
        service_type_ids = self.get_service_type_ids(n_new_services)
        for service_type_id in service_type_ids:
            self.add_new_service(service_type_id, order_id)

    def add_new_service(self, service_type_id, order_id):
        new_service_id = len(self.df) + 1
        price = self.get_service_price(service_type_id)
        new_service = {
            "service_id": new_service_id,
            "order_id": order_id,
            "price": price,
            "start_date": None,
            "end_date": None,
            "service_type_id": service_type_id,
        }
        self.df = self.df.append(new_service, ignore_index=True)
        if self.verbose:
            print("New service:", new_service)

    def start_service(self, service_id, date):
        end_date = self.get_end_of_service_date(service_id, date)
        is_started_service_id = (self.df["service_id"] == service_id)
        self.df.loc[is_started_service_id, "start_date"] = str(date)
        self.df.loc[is_started_service_id, "end_date"] = str(end_date)

        if self.verbose:
            print(f"Start service {service_id} [{date} : {end_date}]")

    def get_actual_service_ids(self, date):
        is_actual = (
            (self.df["end_date"] >= str(date)) | self.df["end_date"].isna()
        )
        return self.df[is_actual]["service_id"]

    def get_not_started_service_ids(self, date):
        not_started = self.df["start_date"].isna()
        return self.df[not_started]["service_id"]

    def get_n_actual_services(self, date):
        return len(self.get_actual_service_ids(date))

    def get_service_ids_ended_between(self, start_date, end_date):
        is_between = (
            (str(start_date) <= self.df["end_date"]) &
            (self.df["end_date"] <= str(end_date))
        )
        return self.df[is_between]["service_id"]

    def get_finished_service_ids(self, date):
        finished = self.df["end_date"] == str(date)
        return self.df[finished]["service_id"]

    def get_prices(self, services_id):
        prices = []
        for service_id in services_id:
            prices.append(self.get_price(service_id))
        return prices

    def get_price(self, service_id):
        return self.df[self.df["service_id"] == service_id]["price"].squeeze()

    def get_end_of_service_date(self, service_id, date):
        days_of_service = int(self.rng.choice([0, 1]))
        return date + datetime.timedelta(days=days_of_service)

    def get_service_price(self, service_type_id):
        return np.abs(
            self.rng.normal(self.service_type_id_to_price[service_type_id], 20)
        )

    def get_service_type_id(self):
        return self.rng.choice(self.service_types["service_type_id"])

    def get_n_of_new_services(self):
        return self.rng.choice([1, 2])

    def get_service_type_ids(self, n_services):
        return self.rng.choice(
            self.types_df["service_type_id"], n_services, replace=False
        )

    def get_service_details(self, service_id):
        data_row = self.df[self.df.service_id == service_id].iloc[0]
        return data_row

    def prepare_tables(self):
        warrancy_period = datetime.timedelta(days=365)
        end_date = pd.to_datetime(self.df["end_date"])
        self.df["warranty_end"] = end_date + warrancy_period
