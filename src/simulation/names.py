from io import StringIO
from functools import lru_cache

import numpy as np
import pandas as pd
import requests

FEMALE_NAMES_URL = "https://api.dane.gov.pl/media/resources/20200130/lista_pierwszych_imion_żeńskich_uwzgl_os_zmarłe_2020-01-21.csv" # noqa
FEMALE_SURNAMES_URL = "https://api.dane.gov.pl/media/resources/20200330/Wykaz_nazwisk_żeńskich_uwzgl_os__zmarłe_2020-01-22.csv" # noqa
MALE_NAMES_URL = "https://api.dane.gov.pl/media/resources/20200130/lista_pierwszych_imion_męskich_uwzgl_os_zmarłe_2020-01-21.csv" # noqa
MALE_SURNAMES_URL = "https://api.dane.gov.pl/media/resources/20200330/Wykaz_nazwisk_męskich_uwzgl_os__zmarłe_2020-01-22.csv" # noqa

RANDOM_SEED = 2020


def get_dataframe(csv_url: str):
    r = requests.get(csv_url)
    csv_data = StringIO(r.text)
    return pd.read_csv(csv_data)


def get_names(url: str):
    df = get_dataframe(url)
    df["p"] = df["LICZBA_WYSTĄPIEŃ"] / df["LICZBA_WYSTĄPIEŃ"].sum()
    df = df.rename(columns={"IMIĘ_PIERWSZE": "name"})
    return df[["name", "p"]]


def get_surnames(url: str):
    df = get_dataframe(url)
    df["p"] = df["Liczba"] / df["Liczba"].sum()
    df = df.rename(columns={"Nazwisko aktualne": "surname"})
    return df[["surname", "p"]]


@lru_cache
def get_female_names():
    return get_names(FEMALE_NAMES_URL)


@lru_cache
def get_female_surnames():
    return get_surnames(FEMALE_SURNAMES_URL)


@lru_cache
def get_male_names():
    return get_names(MALE_NAMES_URL)


@lru_cache
def get_male_surnames():
    return get_surnames(MALE_SURNAMES_URL)


class NameGenerator:
    def __init__(self, random_seed=None):
        self.male_names = get_male_names()
        self.male_surnames = get_male_surnames()
        self.female_names = get_female_names()
        self.female_surnames = get_female_surnames()

        random_seed = RANDOM_SEED if random_seed is None else random_seed
        self.rng = np.random.default_rng(random_seed)

    def generate_name_surname(self, male_chance=.5):
        if self.rng.uniform(0, 1) < male_chance:
            names_df = self.male_names
            surnames_df = self.male_surnames
        else:
            names_df = self.female_names
            surnames_df = self.female_surnames

        name = self.rng.choice(
            names_df['name'], p=names_df['p']
        )

        surname = self.rng.choice(
            surnames_df['surname'], p=surnames_df['p']
        )

        return name, surname
