from dataclasses import dataclass
from .names import NameGenerator

import pandas as pd
import numpy as np

RANDOM_SEED = 2020

CLIENTS_DF = pd.DataFrame(
    {
        "client_id": [],
        "name": [],
        "surname": [],
        "telephone": [],
    }
)


@dataclass
class Clients:
    df: pd.DataFrame = CLIENTS_DF
    verbose: bool = True
    name_gen: NameGenerator = NameGenerator()
    rng = np.random.default_rng(RANDOM_SEED)

    def get_new_client_ids(self, n_new_clients):
        new_client_ids = []
        for _ in range(n_new_clients):
            new_client_id = self.get_new_client_id()
            new_client_ids.append(new_client_id)
        return new_client_ids

    def get_new_client_id(self):
        new_client_id = len(self.df) + 1
        name, surname = self.name_gen.generate_name_surname()
        telephone = self.rng.integers(100_000_000, 999_999_999)
        new_client = {
            "client_id": new_client_id,
            "name": name,
            "surname": surname,
            "telephone": telephone
        }
        self.df = self.df.append(new_client, ignore_index=True)
        if self.verbose:
            print("New client:", new_client)
        return new_client_id
