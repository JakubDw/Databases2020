from collections import defaultdict
from dataclasses import dataclass

import pandas as pd


SALARIES_DF = pd.DataFrame(
    {
        "salary_id": [],
        "salary": [],
        "employee_id": [],
        "payment_date": pd.to_datetime([]),
    }
)


@dataclass
class Salaries:
    df: pd.DataFrame = SALARIES_DF
    verbose: bool = True

    def pay_salaries(self, salaries_per_employee_id, date):
        salaries = self._pay_salaries(salaries_per_employee_id, date)

        return salaries

    def _pay_salaries(self, salaries_per_employee_id, date):
        salaries = []
        for employee_id, salary in salaries_per_employee_id.items():
            salary = self.pay_salary(employee_id, salary, date)
            salaries.append(salary)

        return salaries

    def pay_salary(self, employee_id, salary, date):
        new_salary_id = len(self.df) + 1
        new_salary = {
            "salary_id": new_salary_id,
            "salary": salary,
            "employee_id": employee_id,
            "payment_date": str(date),
        }
        self.df = self.df.append(new_salary, ignore_index=True)
        if self.verbose:
            print("New salary:", new_salary)

        return salary


def accumulate(employee_ids, salaries):
    salaries_per_employee_id = defaultdict(int)
    for employee_id, salary in zip(employee_ids, salaries):
        salaries_per_employee_id[int(employee_id)] += salary
    return salaries_per_employee_id
