#!/usr/bin/env python3
import argparse
import datetime

import jupytext
import nbconvert

ANALYSIS_PATH = "/home/jovyan/src/analysis/renovations_analysis.md"
REPORT_PATH = "/home/jovyan/reports/{date}-report.html"


def insert_password(notebook, password):
    for cell in notebook["cells"]:
        tags = cell["metadata"].get("tags")
        if tags is not None:
            if "replace_password" in tags:
                cell["source"] = f"SQL_PASSWORD = '{password}'"


def execute_notebook(notebook):
    executer = nbconvert.preprocessors.ExecutePreprocessor(
        timeout=600, allow_errors=True, kernel_name='python3'
    )
    executer.preprocess(notebook)


def get_html(notebook):
    html_exporter = nbconvert.HTMLExporter()
    html_exporter.exclude_input = True
    html_exporter.exclude_output_prompt = True
    html_exporter.exclude_input_prompt = True
    body, resource = html_exporter.from_notebook_node(notebook)
    return body


def save_notebook(notebook):
    filepath = REPORT_PATH.format(date=str(datetime.date.today()))
    html = get_html(notebook)
    with open(filepath, "wt") as f:
        f.write(html)


def main(password: str):
    notebook = jupytext.read(ANALYSIS_PATH)
    insert_password(notebook, password)
    execute_notebook(notebook)
    save_notebook(notebook)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--password", help="database password")
    args = parser.parse_args()

    if args.password:
        main(args.password)
    else:
        parser.print_help()
