#!/usr/bin/env python3
import argparse
from typing import Iterable, Tuple

import pandas as pd
import sqlalchemy

from simulate import generate_tables


def fill_database(
    tables: Iterable[Tuple[str, pd.DataFrame]],
    engine: sqlalchemy.engine
):
    for table_name, df in tables:
        df.to_sql(table_name, engine, if_exists="append", index=False)


def get_engine(user: str, password: str):
    return sqlalchemy.create_engine(
        f"mysql+pymysql://{user}:{password}@db/renovations?charset=utf8mb4"
    )


def main(user: str, password: str):
    engine = get_engine(user, password)
    tables = generate_tables()
    fill_database(tables, engine)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--password", help="database password")
    parser.add_argument("-u", "--username", help="database username")
    args = parser.parse_args()

    if (args.username and args.password):
        main(args.username, args.password)
    else:
        parser.print_help()
