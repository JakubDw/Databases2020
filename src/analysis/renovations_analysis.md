---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.9.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import os
import pymysql
import jupytext
import datetime
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from datetime import date, timedelta

pd.options.plotting.backend = "plotly"
```

```python tags=["replace_password"]
SQL_PASSWORD = os.environ.get('MYSQL_ROOT_PASSWORD')
```

```python
connection = pymysql.connect(host='db',
                             user='root',
                             password=SQL_PASSWORD,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
```

# Liczba zleceń oczekujących w czasie

```python
wait_sql = """SELECT order_id, order_date, MIN(start_date) AS realization_date
          FROM `renovations`.`services`
          INNER JOIN `renovations`.`orders`
          USING (order_id)
          GROUP BY order_id;"""
```

```python
# creating df for waiting orders:
wait = pd.read_sql(wait_sql, connection)
```


```python
def dates_range(df: pd.DataFrame, start_date: str, end_date: str):
    """Returns a range of datetime objects"""
    first_order = min(df[start_date])
    last_realized_order = max(df[end_date])
    return pd.date_range(first_order, last_realized_order)

def waiting_orders(df, start_date: str, end_date: str):
    """
    Returns DataFrame with daily number of waiting orders 
    
    Args:
        df (DataFrame): dataframe with order_date and realization_date
    Returns:
        daily_waits (DataFrame): dataframe with daily counts of waiting orders
                    
    """
    daily_waits = pd.DataFrame(
        index=dates_range(df, start_date=start_date, end_date=end_date)
    )
    daily_waits["counts"] = 0
    
    for id, row in df.iterrows():
        daily_waits.loc[str(row[start_date])] += 1 # adds one in case if someone makes an order in a particular day
        daily_waits.loc[str(row[end_date])] += -1 # adds -1 if the realization of order begins
    daily_waits["total_count"] = daily_waits["counts"].cumsum()
    return daily_waits
```

```python
daily_waiting_time = waiting_orders(wait, start_date="order_date", end_date="realization_date")
```

```python
daily_waiting_plot = go.Figure(layout_title_text="Liczba oczekujących zamówień w czasie")
daily_waiting_plot.add_trace(go.Scatter(
    x=daily_waiting_time.index,
    y=daily_waiting_time.total_count,
    mode='lines+markers',
))

daily_waiting_plot.show()
```

# Liczba aktywnych pracowników w czasie

```python
emp_sql = """SELECT *
             FROM `renovations`.`employees`;"""
```

```python
employees = pd.read_sql(emp_sql, connection)
```


```python
employees.loc[pd.isnull(employees["end_date"]), ["end_date"]]= str(date.today())
```


```python
active_emp = waiting_orders(employees, start_date="start_date", end_date="end_date")
```


```python
active_emp_plot = go.Figure(layout_title_text="Liczba aktywnych pracowników w czasie")
active_emp_plot.add_trace(go.Scatter(
    x=active_emp.index[:-2],
    y=active_emp.total_count[:-2],
    mode='lines+markers',
))

active_emp_plot.show()
```

# Bilans finansowy

```python
transac_sql = """SELECT *
                 FROM `renovations`.`transactions`
                 LEFT JOIN `renovations`.`transaction_types`
                 USING(trans_type_id);"""
```

```python
transac = pd.read_sql(transac_sql, connection)
```

```python
transac["Balance"] = transac["trans_amount"].cumsum()
```


```python
transactions_plot = go.Figure(layout_title_text="Bilans finansowy")
transactions_plot.add_trace(go.Scatter(
    x=transac.trans_date,
    y=transac.Balance,
    mode='lines+markers'
))

transactions_plot.show()
```

# Najdłużej oczekujący klienci

```python
wait_client_sql = """SELECT CONCAT_WS(' ', name, surname) AS name_surname, 
                            MAX(DATEDIFF(start_date, order_date)) AS max_waiting_time
                     FROM `renovations`.`services`
                          LEFT JOIN `renovations`.`orders` USING(order_id)
                          LEFT JOIN `renovations`.`clients` USING(client_id)
                     GROUP BY client_id
                     ORDER BY max_waiting_time DESC;"""
```

```python
wait_client = pd.read_sql(wait_client_sql, connection)
```


```python
y = (wait_client[wait_client["max_waiting_time"]>0])[:5]["max_waiting_time"]
fig = go.Figure(layout_title_text="Klienci oczekujący na zamówienie najdłużej",
            data=[go.Bar(
            x=(wait_client[wait_client["max_waiting_time"]>0])[:5]["name_surname"], 
            y=y,
            text=y,
            textposition='auto',
        )])

fig.update_layout(
                 yaxis_title="Liczba dni",
                 yaxis_showticklabels=False
)

fig.show()
```

# Ilość świadczonych usług

```python
serv_quant_sql = """SELECT service_type, COUNT(*) AS frequency
                    FROM `renovations`.`services`
                    LEFT JOIN `renovations`.`services_types`
                    USING(service_type_id)
                    GROUP BY service_type;"""
```

```python
serv_quant = pd.read_sql(serv_quant_sql, connection)
```


```python
serv = px.pie(serv_quant, values='frequency', names='service_type', title='Ilość świadczonych usług')
serv.show()
```

# Najczęściej wykorzystywane narzędzia

```python
equip_quant_sql = """SELECT service_type, equipment_type, COUNT(*) AS frequency
                     FROM `renovations`.`services`
                     LEFT JOIN `renovations`.`services_types`
                     USING(service_type_id)
                     LEFT JOIN `renovations`.`equipment_services`
                     USING(service_id)
                     LEFT JOIN `renovations`.`equipment`
                     USING(equipment_id)
                     LEFT JOIN `renovations`.`equipment_types`
                     USING(equipment_type_id)
                     GROUP BY service_type, equipment_type;"""
```

```python
equip_quant = pd.read_sql(equip_quant_sql, connection)
```


```python
equip = px.treemap(
    equip_quant,
    path=['service_type', 'equipment_type'],
    values='frequency',
    title="Częstotliwość korzystania z narzędzi dla poszczególnych usług"
)
equip.show()
```

# Zmiana zarobków w czasie

```python
salaries_sql = """SELECT *
                  FROM `renovations`.`salaries`;"""
```

```python
salaries = pd.read_sql(salaries_sql, connection)
```


```python
sal_grouped = salaries[["payment_date", "salary"]].groupby("payment_date").mean()
```

```python
sal = go.Figure(layout_title_text="Średnia miesięczna wysokość zarobków")

sal.add_trace(go.Scatter(name='line of best fit', x=sal_grouped.index, y=sal_grouped['salary'], mode='lines'))

sal.update_layout(
                 yaxis_title="",
                 yaxis_showticklabels=True,
                 showlegend=False
)

sal.show()
```

# Zmiana cen usług w czasie

```python
serv_price_sql = """SELECT start_date, 
                    AVG(price) AS average_price
                    FROM `renovations`.`services`
                    GROUP BY start_date;"""
```

```python
serv_price = pd.read_sql(serv_price_sql, connection)
```

```python
serv_price["start_date"] = pd.to_datetime(serv_price["start_date"])
```

```python
serv_price = serv_price.resample('M', on='start_date').mean()
```

```python
serv_price_plot = go.Figure(layout_title_text="Zmiana cen usług w czasie")
serv_price_plot.add_trace(go.Scatter(
    x=serv_price.index,
    y=serv_price["average_price"],
    mode='lines+markers'
))

serv_price_plot.show()
```

# Dzielnice, w których realizowanych jest najwięcej zleceń

```python
area_service_sql = """SELECT DATE_FORMAT(order_date, '%Y-%m') AS order_date, 
       district, 
       COUNT(order_id) AS orders_volume
FROM `renovations`.`orders`
GROUP BY DATE_FORMAT(order_date, '%Y-%m'), 
         district;"""
```

```python
area_service = pd.read_sql(area_service_sql, connection)
```

```python
area = px.area(area_service, x="order_date", y="orders_volume", color="district", title="Dzielnice, w których realizowanych jest najwięcej zleceń")
area.show()
```

```python

```
