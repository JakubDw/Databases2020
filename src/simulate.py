import argparse
from datetime import date
from pathlib import Path
from typing import Iterable, Tuple

import pandas as pd

from simulation.simulation import Simulation

DEFAULT_START_DATE = date(2018, 1, 1)


def simulate(start_date, end_date):
    n_days = (end_date - start_date).days
    simulation = Simulation(start_date)
    simulation.simulate(n_days)
    return simulation


def generate_tables(start_date=DEFAULT_START_DATE, end_date=date.today()):
    simulation = simulate(start_date, end_date)
    return simulation.get_tables()


def save_tables(tables: Iterable[Tuple[str, pd.DataFrame]], directory: Path):
    directory.mkdir(exist_ok=True)
    for table_name, df in tables:
        filepath = directory / f"{table_name}.csv"
        df.to_csv(filepath, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--start-date",
        help="Start date in ISO format (YYYY-MM-DD).",
        required=True,
        type=date.fromisoformat,
    )
    parser.add_argument(
        "-e", "--end-date",
        help="End date in ISO format (YYYY-MM-DD).",
        type=date,
        default=date.today(),
    )
    parser.add_argument(
        "-d",
        "--dir",
        type=Path,
        help="directory to store csv files instead of filling database.",
    )
    args = parser.parse_args()
    simulation = simulate(args.start_date, args.end_date)
    if args.dir:
        tables = simulation.get_tables()
        save_tables(tables, args.dir)
