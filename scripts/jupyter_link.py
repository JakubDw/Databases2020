#!/usr/bin/env python3
import json
import subprocess

TOKEN_COMMAND = "docker-compose exec python jupyter-notebook list --json"
PORT_COMMAND = "docker-compose port python 8888"


def get_token():
    result = subprocess.check_output(TOKEN_COMMAND.split())
    jupyter_config = json.loads(result.decode())
    return jupyter_config["token"]


def get_port_binding():
    result = subprocess.check_output(PORT_COMMAND.split())
    port = result.decode().split(":")[-1].strip()
    return port


def get_jupyter_link():
    token = get_token()
    port = get_port_binding()
    return f"http://localhost:{port}/?token={token}"


if __name__ == "__main__":
    print(get_jupyter_link())
